package tcp;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileStreamTest {
	 public static void main(String args[]){
		    try{
		      byte bWrite [] = {11,21,3,40,5};  //字节数组
		      OutputStream os = new FileOutputStream("test.txt");   //使用字符串类型的文件名来创建一个输出流对象,写入文件
		      for(int x=0; x < bWrite.length ; x++){
		      os.write( bWrite[x] ); // writes the bytes
		    }
		    os.close();
		 
		    InputStream is = new FileInputStream("test.txt");  //使用字符串类型的文件名来创建一个输入流对象来读取文件
		    int size = is.available();
		 
		    for(int i=0; i< size; i++){
		      System.out.print((char)is.read() + "  ");    //读取字节
		    }
		      is.close();
		    }catch(IOException e){
		      System.out.print("Exception");
		    }  
		  }

}
