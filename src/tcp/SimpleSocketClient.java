package tcp;
import java.io.*;
import java.net.*;
/**
 * 简单的Socket客户端
 * 功能为：发送字符串“Hello”到服务器端，并打印出服务器端的反馈
 */
public class SimpleSocketClient {
         public static void main(String[] args) {
                   Socket socket = null;
                   InputStream is = null;  //输入
                   OutputStream os = null;  //输出
                   //服务器端IP地址ַ
                   String serverIP = "127.0.0.1";
                   //服务器端端口号
                   int port = 10000;
                   //发送内容
                   String data = "Hello";
                   try {
                            //1.建立连接
                            socket = new Socket(serverIP,port);
                            //2.发送数据
                            //由系统自动完成将输出流中的数据发送出去，如果需要强制发送，可以调用输出流对象中的flush方法实现
                            os = socket.getOutputStream(); //获得输出流
                            os.write(data.getBytes());   //public void write(byte[] w)把指定数组中w.length长度的字节写到OutputStream中。
                            //3.接收数据
                            is = socket.getInputStream();
                            byte[] b = new byte[1024];
                          //  public int read(byte[] r) throws IOException{}
                          //  这个方法从输入流读取r.length长度的字节。返回读取的字节数。如果是文件结尾则返回-1
                            int n = is.read(b);
                            //输出反馈数据
                            System.out.println("服务器反馈" + new String(b,0,n));
                   } catch (Exception e) {
                            e.printStackTrace(); //打印异常信息
                   }finally{
                            try {
                                     //关闭流和连接
                                     is.close();
                                     os.close();
                                     socket.close();
                            } catch (Exception e2) {}
                   }
         }
}