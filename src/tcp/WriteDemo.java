package tcp;
/**
 * 控制台输出
 * @author Administrator
 * write() 方法不经常使用，因为 print() 和 println() 方法用起来更为方便。
 */
public class WriteDemo {
	
	public static void main(String args[]) {
	      int b; 
	      b = 'A';
	      System.out.write(b);
	      System.out.write('\n');
	   }

}
